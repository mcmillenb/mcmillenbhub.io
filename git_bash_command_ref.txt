Clone a repo:
mkdir /path
cd /path
git clone https://github.com/[username]/[reponame]

Add and commit file:
git add [filename]
git commit [filename] -m "[commit message]"
git push origin master

Delete file:
git rm [filename]
(committing is the same)

If you forgot -m:
It opens an editor,
type your message
then ESC+":wq"

Press Tab to autocomplete